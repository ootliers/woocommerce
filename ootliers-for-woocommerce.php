<?php

/**
 * @link
 * @since             1.0.0
 * @package           Ootliers
 *
 * @wordpress-plugin
 * Plugin Name:       Ootliers For Woocommerce
 * Plugin URI:        https://www.ootliers.com/?utm_source=wp_plugin&utm_campaign=plugin_list
 * Description:       Integration with the order monitoring system Ootliers.
 * Version:           1.0.0
 * Author:            Ootliers
 * Author URI:
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sync-orders
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

define('OOTWC_VERSION', '1.0.0');



require plugin_dir_path(__FILE__) . 'includes/ootwc_main.php';

load_plugin_textdomain(
    'ootliers',
    false,
    dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
);

function ootwc_send_order($order)
{
    $soapUrl = "https://api.ootliers.com/api/v1/site/" . get_option('te_Site_id') . "/order";
    $url = $soapUrl;
    $data = $order->get_data();
    $items = $order->get_items();
    $headers = array(
        "Content-Type: application/json",
        'Authorization: ' . get_option('te_Site_key'),
        'User-Agent: WP-Ootliers/' . OOTWC_VERSION,
    );
    $items_data = [];

    foreach ($items as $item_id => $item) {
        $items_data[] = [
            'name' => $item->get_name(),
            'price' => $item->get_total(),
            'quantity' => $item->get_quantity(),
        ];
    }

    $order_data = [
        'order' => [
            'number' => $order->get_id(),
            'ordered_at' => $data['date_created']->date(\DATE_ATOM),
            'total' => $order->get_total(),
            'items' => $items_data,
        ]
    ];

    $bodyAsJson = json_encode($order_data);

    $response = wp_remote_post($url, ['body' => $bodyAsJson, 'headers' => $headers]);

    return $response;
}

$plugin = new OOTWC_Main();
$plugin->run();
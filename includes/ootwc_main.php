<?php

class OOTWC_Main {

	protected $plugin_name;

	protected $version;

	public function __construct() {
		if ( defined( 'OOTWC_VERSION' ) ) {
			$this->version = OOTWC_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'ootliers';
	}

	private function load_dependencies() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/ootwc_admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/ootwc_public.php';
	}

	private function define_admin_hooks() {
		$plugin_admin = new OOTWC_Admin( );

		add_action( 'admin_menu', [$plugin_admin, 'setting_page_fun_for_sync'] );
		add_action( 'admin_notices', [$plugin_admin, 'admin_notice_about_bulk_action'] );
		add_action( 'admin_footer', [$plugin_admin, 'footer'] );
		add_action( 'admin_init', [$plugin_admin, 'init'] );
	}

	private function define_public_hooks() {
		$plugin_public = new OOTWC_Public( );
		add_action( 'woocommerce_checkout_update_order_meta', [$plugin_public, 'save_data']);
	}

	public function run() {
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
	}
}

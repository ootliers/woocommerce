<?php

class OOTWC_Admin
{
    public function setting_page_fun_for_sync()
    {
        add_menu_page(
            __('Ootliers Setting', 'textdomain'),
            'Ootliers',
            'manage_options',
            'sync-order',
            array($this, 'setting_page_for_sync_order'),
            ' dashicons-admin-generic'
        );
    }

    public function setting_page_for_sync_order()
    {
        if (isset($_POST['submitSyncSetting'])) {
            $Site_id = sanitize_key($_POST['Site_id']);
            $te_Site_key = sanitize_key($_POST['te_Site_key']);
            update_option('te_Site_id', $Site_id);
            update_option('te_Site_key', $te_Site_key);
            echo '<h1 style="color:white; background-color:#003a92; width:50%; padding:20px; border-radius:10px; text-align:center;">Settings Values Updated</h1>';
        }

        $html = '<form method="POST" action="">
			<table class="form-table" role="presentation">
				<tbody>
					<tr>
						<th><label for="Site_id">Site Id</label></th>
						<td> <input name="Site_id" id="Site_id" type="text" value="' . esc_attr(get_option('te_Site_id')) . '" class="regular-text code"></td>
					</tr>
					<tr>
						<th><label for="te_Site_key">Key</label></th>
						<td> <input name="te_Site_key" id="te_Site_key" type="text" value="' . esc_attr(get_option('te_Site_key')) . '" class=" regular-text code"></td>
					</tr>
					<tr>
						<th>
							<p class="submit">
								<input type="submit" name="submitSyncSetting" id="submitSyncSetting" class="button button-primary" value="Save Changes">
							</p>
						</th>
					</tr>
					<tr>
					    <td colspan="2">Don\'t have an Ootliers account? Get one at <a href="https://www.ootliers.com/?utm_source=wp_plugin&utm_campaign=plugin_convert">https://www.ootliers.com</a></td>
					</tr>
				</tbody>
			</table>
		</form>';
        echo $html;
    }

    public function admin_notice_about_bulk_action()
    {
        if (!empty($_REQUEST['all_order_sync_succesfuly'])) {
            echo '<div id="message" class="updated notice is-dismissible" style="background:#000daf;color:white;">
			<p>All orders Sync Successfully. Thanks.</p>
		</div>';
        }

        if (!empty($_REQUEST['something_wrong_order_sync'])) {

            echo '<div id="message" class="notice is-dismissible" style="background:orange;color:white;">
			<p>' . esc_html($_REQUEST['all_order_sync_succesfuly']) . '</p>
		</div>';

        }
    }

    public function footer()
    {
        $screen = get_current_screen();
        if ($screen->id == 'edit-shop_order' || $screen->id == 'shop_order') {


            ?>
            <script>
                jQuery(document).ready(function () {


                    jQuery('a.page-title-action').after('<a href="<?php echo admin_url('edit.php?post_type=shop_order&aw_synch=true'); ?>" class="add-new-h2">Send Orders To Ootliers</a>');

                });
            </script>
            <?php
        }
    }

    public function init()
    {
        if (isset($_GET['aw_synch'])) {
            $i = 1;
            $dateTime = new \DateTime('-2 months');
            $query = new WC_Order_Query(array(
                'limit' => -1,
                'orderby' => 'date',
                'order' => 'DESC',
                'return' => 'ids',
                'date_created' => '>='.$dateTime->format('Y-m-d'),
            ));
            $orders = $query->get_orders();

            $soapUrl = "https://api.ootliers.com/api/v1/site/" . get_option('te_Site_id') . "/order";
            $url = $soapUrl;

            foreach ($orders as $aw_order) {
                $order = wc_get_order($aw_order);
                $response = ootwc_send_order($order);
                if ($response) {
                    $success_count = $i;
                    $i++;
                }
            }

            wp_redirect(admin_url("edit.php?post_type=shop_order&all_order_sync_succesfuly={$i}"));

        }
    }

}

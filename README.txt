=== Ootliers For Woocommerce ===
Contributors: ootliers
Tags: woocommerce, montioring
Requires at least: 5.0
Tested up to: 5.7.1
WC requires at least: 5.0
WC tested up to: 5.2.2
Requires PHP: 7.0
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Monitor your sales performance with Ootliers. Make sure your shop is making as much money as possible.

== Description ==

Ootliers monitors your sales performances and instantly notifies you if sales drop! This plugin integrates with Woocommerce to allow you to have peace of mind with your WooCommerce store

== Frequently Asked Questions ==

= Q. Is Ootliers For Woocommerce free?? =
A. The plugin is free but to use the monitoring service is not. You can buy the monitoring service at [https://www.ootliers.com](https://www.ootliers.com/?utm_source=wordpress_org&utm_campaign=plugin)

== Installation ==

1. Upload `plugin` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Input API Key and Site ID
4. Go to orders page and click sync with Ootliers

== Changelog ==

= 1.0.0 =

* Inital release